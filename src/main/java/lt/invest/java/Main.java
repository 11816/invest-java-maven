package lt.invest.java;

import jooq.tables.records.ArtistRecord;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static jooq.tables.Artist.ARTIST;

public class Main {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/Sample";
		String user = "bit";
		String password = "invest";

		try(Connection con = DriverManager.getConnection(url, user, password)){
			var context = DSL.using(con, SQLDialect.MYSQL);
			Result<ArtistRecord> result = context.selectFrom(ARTIST).fetch();
			for(ArtistRecord record : result) {
				System.out.printf("%d %s\n", record.getArtistid().intValue(), record.getName());
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}
